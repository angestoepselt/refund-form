ARG ALPINE_VERSION=3.18
FROM alpine:${ALPINE_VERSION}
LABEL Maintainer="Matthias Hemmerich matthias+code@mailbro.de"
LABEL Description="Forked from https://github.com/TrafeX/docker-php-nginx, Lightweight container with Nginx 1.20 & PHP 8.0 based on Alpine Linux."

# Setup document root
WORKDIR /var/www/html

# Install packages
RUN apk update \
  && apk add --no-cache \
  curl \
  nginx \
  php82 \
  php82-fpm \
  supervisor

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php82/php-fpm.d/www.conf
COPY config/php.ini /etc/php82/conf.d/custom.ini

RUN mkdir /etc/supervisor.d \
  && \
  chown -R nobody.nobody /var/www/html /run /var/lib/nginx /var/log/nginx

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor.d/supervisord.conf

# Add application
COPY --chown=nobody app/ /var/www/html/

# Switch to use a non-root user from here on
USER nobody

# Expose the port nginx is reachable on
EXPOSE 80

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up & running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:80/fpm-ping