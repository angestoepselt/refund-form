$(document).ready(function() {
    /* Get form data on print page */
    var url = window.location.href;
    var str = 'print';
    if(url.includes(str)) {
        if (localStorage.getItem('name') !== null) {
            getFormData();
        } else {
            window.location.href = 'index.html';
        }
    }

    /* Clone services input on plus and remove on minus button click */
    var count = 1;
    $('.btn-plus').on('click', function() {
        var html='' +
            '<div id="form-leistung_' + count + '" class="form-leistung position-relative mb-3">' +
                '<div class="row">' +
                    '<div class="col col-10">' +
                        '<input id="leistung_' + count + '" name="leistung_' + count + '" type="text" class="leistung form-control">' +
                    '</div>' +
                    '<div class="col col-2">' +
                        '<input id="betrag_' + count + '" name="betrag_' + count + '" type="text" class="betrag form-control">' +
                    '</div>' +
                '</div>' +
                '<button onclick=remove("' + count + '") class="btn-minus position-absolute">&#8722;</button>' +
            '</div>';
        $("#form-leistung-wrapper").append(html);

        count++;
    })


    /* Get total amount */
    $(document).on('change', '.form-leistung .betrag', function(){
        $('#gesamt').val(getTotal());
    });
    $(document).on('click', '.btn-minus', function(){
        $('#gesamt').val(getTotal());
    });


    /* Show iban field if checkbox is checked, hide if unchecked */
    if ($('input#auszahlung').prop('checked')) {
        $('#form-iban').fadeIn();
    } else {
        $('#form-iban').fadeOut();
    }

    $('input#auszahlung').change(function() {
        if ($(this).prop('checked')) {
            $('#form-iban').fadeIn();
        } else {
            $('#form-iban').fadeOut();
        }
    });

    $('.btn-continue').on('click', function() {
        validate();
    })
});

function remove(which) {
    $("#form-leistung_" + which).remove();
}

function getTotal() {
    var total = 0;
    $.each($('.form-leistung .betrag'), function(index, item) {
        total += parseFloat($(item).val().replace(',', '.'));
    });

    return Number(total).toFixed(2);
}

function storeFormData() {
    console.log("here");
    var name = $('#name').val();
    var adresse = $('#adresse').val();
    var citydate = $('#citydate').val();
    var leistung = {};
    var betrag = {};
    $('.form-leistung').each(function(index, item) {
        leistung[index] = $(item).find('.leistung').val();
        betrag[index] = $(item).find('.betrag').val();

        localStorage.setItem('leistung', JSON.stringify(leistung));
        localStorage.setItem('betrag', JSON.stringify(betrag));
    })

    localStorage.setItem('name', name);
    localStorage.setItem('adresse', adresse);
    localStorage.setItem('citydate', citydate);

    var gesamt = $('#gesamt').val();
    localStorage.setItem('gesamt', gesamt);

    if($("#verzicht").is(':checked')) {
        localStorage.setItem('verzicht', 1);
    } else {
        localStorage.setItem('verzicht', 0);
    }

    if($("#auszahlung").is(':checked')) {
        var iban = $('#iban').val();

        localStorage.setItem('auszahlung', 1);
        localStorage.setItem('iban', iban);
    } else {
        localStorage.setItem('auszahlung', 0);
        localStorage.setItem('iban', '');
    }

    window.location.href = 'print.html';
}

function getFormData() {
    var name = localStorage.getItem('name');
    var adresse = localStorage.getItem('adresse');
    var verzicht = localStorage.getItem('verzicht');
    var auszahlung = localStorage.getItem('auszahlung');
    var iban = localStorage.getItem('iban');
    var citydate = localStorage.getItem('citydate');
    var gesamt = localStorage.getItem('gesamt');

    $('#name').text(name);
    $('#adresse').text(adresse);
    $('#citydate').text(citydate);

    var leistung = JSON.parse(localStorage.getItem('leistung'));
    var betrag = JSON.parse(localStorage.getItem('betrag'));

    for(var k1 in leistung) {
        $('#leistung').append('<div>' + leistung[k1] + '</div>');
    }

    for(var k2 in betrag) {
        $('#betrag').append('<div>' + betrag[k2] + ' \u20AC' + '</div>');
    }

    $('#gesamt').text(gesamt.replace('.', ',') + ' \u20AC');

    if(verzicht == 1) {
        $('#verzicht').text('Ich verzichte auf die Erstattung des Betrages und spende ihn stattdessen');
    }

    if(auszahlung == 1) {
        $('#auszahlung').text('Ich bitte um Auszahlung des Betrages per Banküberweisung auf mein Konto:');
        $('#iban').text(iban);
    }
}

function validate() {
    let valid = true;
    $('[required]').each(function() {
        if ($(this).is(':invalid') || !$(this).val()) valid = false;
    })
    if (!valid) alert('Bitte alle Pflichtfelder ausfüllen')
    else storeFormData();
}